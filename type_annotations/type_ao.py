import requests
import datetime as dt
from typing import Callable


class Vehicle:

    def __init__(self, p_make: str, p_model: str, p_fuel: str | None = None):
        self.make = p_make
        self.model = p_model
        self.fuel = p_fuel


class Car(Vehicle):
    def __init__(self, s_make: str, s_model: str, s_fuel: str | None = None) -> None:
        super().__init__(s_make, s_model, s_fuel)

    def describe(self):
        if self.fuel is None:
            return f"{self.make} of model {self.model}"
        return f"{self.make} of model {self.model} uses {self.fuel}"


def get_status_code() -> int:
    response = requests.get("https://www.apple.com/fr/")
    status_code = response.status_code
    return status_code


def get_today_time() -> str:
    today_date = f"{dt.datetime.now():%H:%M:%S}"
    return today_date


def repeat(func: Callable, number_of_times: int) -> None:
    for i in range(number_of_times):
        print(f"{i+1} times --> {func}")


def print_it(text: str, func: Callable[[str], str]) -> None:
    print(func(text))


def upper_print(text: str) -> str:
    return text.upper()


class Fruit:
    def __init__(self, name: str, grams: int) -> None:
        self.name = name
        self.grams = grams


def describe(p_fruit: Fruit):
    print(f"fruit {p_fruit.name} weights {p_fruit.grams} grams")


class Apple(Fruit):
    def __int__(self, apple_name: str, apple_grams: int, apple_id: int) -> None:
        self.apple_id = apple_id
        super.__init__(apple_name, apple_grams)

    def get_apple_id(self):
        return self.apple_id

    def describe(self):
        print(f"fruit {self.name} weights {self.grams} grams and id is {self.apple_id}")

