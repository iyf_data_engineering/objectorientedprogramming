from type_ao import Vehicle, Car, get_status_code, repeat, get_today_time
from type_ao import print_it, upper_print
from type_ao import Fruit, describe, Apple
from typing import Any, Final, Iterable, Sequence
from sys import getsizeof


def get_users_id(users_id) -> str | None:
    my_dict: dict = {0: 'Mother', 1: 'Father'}
    return my_dict.get(users_id, "Nothing Found")


def get_size(user_input: Any) -> None:
    print(f"{user_input} -> sizes is {getsizeof(user_input)} bytes")


def get_iterable(elements: dict[int, str]):
    for k, v in enumerate(elements.values()):
        print(f"{k}: {v}")


if __name__ == "__main__":
    orange_fruit: Fruit = Fruit(name="orange", grams=10)

    describe(orange_fruit)

    my_apple: Apple = Apple("Crocked", 10, 50)
    my_apple.describe()

    # my_car: Car = Car(s_make="Toyota", s_model="Thunder")
    # print(isinstance(my_car, Car))
    # print(isinstance(my_car, Vehicle))
    # print(issubclass(Car, Vehicle))
    # print(my_car.describe())
    # print(get_users_id(99))
    # print(get_status_code())
    # get_size([1, 2, 3])
    # print(MY_VALUE)
    # print({10: "John", 20: "James", 30: "Bob"}.values())
    # get_iterable({10: "John", 20: "James", 30: "Bob"})
    # repeat(func="str", number_of_times=3)
    # print_it("hello", upper_print)

    # MY_VALUE: Final[str] = "Mother"
    # MY_VALUE = "Father"
